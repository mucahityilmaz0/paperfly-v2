using UnityEngine;
using UnityEngine.SceneManagement;

public class SwerveInputSystem : MonoBehaviour
{
    private float _lastFrameFingerPositionX;
    private float _moveFactorX;
    private float _lastFrameFingerPositionY;
    private float _moveFactorY;
    private AudioSource _audioSource;
    
    public float MoveFactorX => _moveFactorX; 
    public float MoveFactorY => _moveFactorY;
    private void Start()
    {
        
        _audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            
            if (!_audioSource.isPlaying)
            { 
                _audioSource.Play();   
            }
            _lastFrameFingerPositionX = Input.mousePosition.x;
            _lastFrameFingerPositionY = Input.mousePosition.y;
            transform.position += transform.forward * Time.deltaTime;
        }
        else if(Input.GetMouseButton(0))
        {
            _moveFactorX = Input.mousePosition.x - _lastFrameFingerPositionX;
            _lastFrameFingerPositionX = Input.mousePosition.x;
           _moveFactorY = Input.mousePosition.y - _lastFrameFingerPositionY;
            _lastFrameFingerPositionY = Input.mousePosition.y;
        }
        else if(Input.GetMouseButtonUp(0))
        {
            
            if (_audioSource.isPlaying)
            {
                _audioSource.Stop();   
            }
            /*_moveFactorX = 0f;
            _moveFactorY = 0f;*/
        }
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            Debug.Log("hit");
            SceneManager.LoadScene("MenuScene");
        }
    }
}
