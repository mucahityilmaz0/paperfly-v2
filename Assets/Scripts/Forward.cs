using UnityEngine;
using UnityEngine.SceneManagement;

public class Forward : MonoBehaviour
{
    private float x = 0f;
    private float y = 0f;
    private float z = 0f;
    private void Update()
    {
        Move();
    }
    private void Move()
    {
        if (Input.GetMouseButtonDown(0))
        {
            z += .1f;
            y += 1f;
        }

        if (Input.GetMouseButtonUp(0))
        {
            z -= .1f;
        }
        transform.Translate(x,y,z);
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            Debug.Log("hit");
            SceneManager.LoadScene("MenuScene");
        }
    }
}
