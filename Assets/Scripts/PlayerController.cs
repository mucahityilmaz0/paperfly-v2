using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private SwerveInputSystem _swerveInputSystem;
    private const float MaxSwerveAmountX = 2f;
    private const float MaxSwerveAmountY = 3f;
    private float _swerveAmountX;
    private float _swerveAmountY;
    private float _swerveAmountZ;
    [SerializeField] private float swerveSpeed = 0.5f;

    private Rigidbody _rigidbody;
    
    public float forwardSpeed = 100f, strafeSpeed =100f, hoverSpeed = 50f;
    private float _activeForwardSpeed, _activeStrafeSpeed, _activeHoverSpeed;
    private const float ForwardAcceleration = 25f;
    private const float StrafeAcceleration = 20f;
    private const float HoverAcceleration = 20f;

    public float lockRateSpeed = 50f;
    public float rollSpeed = 50f;
    

    private void Start()
    {
        _swerveInputSystem = GetComponent<SwerveInputSystem>();
        _rigidbody = GetComponent<Rigidbody>();
        Cursor.lockState = CursorLockMode.Confined;
    }

    private void Update()
    {
        _swerveAmountX= Time.deltaTime * swerveSpeed * _swerveInputSystem.MoveFactorX; 
        _swerveAmountY= Time.deltaTime * swerveSpeed * _swerveInputSystem.MoveFactorY;
       // _swerveAmountZ = Time.deltaTime * swerveSpeed * _swerveInputSystem.MoveFactorX;
        var clampedPosition = gameObject.transform.position;
        clampedPosition.x = Mathf.Clamp(clampedPosition.x, -MaxSwerveAmountX, MaxSwerveAmountX);
        clampedPosition.y = Mathf.Clamp(clampedPosition.y, -MaxSwerveAmountY, MaxSwerveAmountY);
        var position = clampedPosition;

        transform.Rotate(-_swerveAmountY * lockRateSpeed * Time.deltaTime,
            -_swerveAmountX * lockRateSpeed * Time.deltaTime, _swerveAmountX * rollSpeed * Time.deltaTime, Space.Self);
        
        _activeForwardSpeed = Mathf.Lerp(_activeForwardSpeed, _swerveAmountY/2 * forwardSpeed,
            ForwardAcceleration );
        _activeStrafeSpeed = Mathf.Lerp(_activeStrafeSpeed, _swerveAmountX * strafeSpeed,
            StrafeAcceleration );
        /*_activeHoverSpeed = Mathf.Lerp(_activeHoverSpeed, _swerveAmountX * hoverSpeed,
            HoverAcceleration );*/

        var transform1 = transform;
        gameObject.transform.position += transform1.forward * (_activeForwardSpeed * Time.deltaTime);
        gameObject.transform.position += transform1.right * (_activeStrafeSpeed * Time.deltaTime);
        transform1.position = gameObject.transform.position;
        if (transform1.position.z<=-4)
        {
            _rigidbody.useGravity = true;
        }

    }
   
   
   /* public float forwardSpeed = 25f, strafeSpeed =7.5f, hoverSpeed = 5f;
    private float _activeForwardSpeed, _activeStrafeSpeed, _activeHoverSpeed;
    private float forwardAcceleration = 2.5f, strafeAcceleration = 2f, hoverAcceleration = 2f;

    public float lockRateSpeed = 50f;
    private Vector2 lookInput,screenCenter,mouseDistance;

    private float rollInput;
    public float rollSpeed = 90f, rollAcceleration = 3.5f;

    void Start()
    {
        screenCenter.x = Screen.width * .5f;
        screenCenter.y = Screen.height * .5f;

        Cursor.lockState = CursorLockMode.Confined;
    }

    void Update()
    {
        lookInput.x = Input.mousePosition.x;
        lookInput.y = Input.mousePosition.y;
        
        mouseDistance.x = (lookInput.x - screenCenter.x) / screenCenter.x;
        mouseDistance.y = (lookInput.y - screenCenter.y) / screenCenter.y;

        mouseDistance = Vector2.ClampMagnitude(mouseDistance, 1f);

        rollInput = Mathf.Lerp(rollInput, Input.GetAxisRaw("Roll"), rollAcceleration * Time.deltaTime);

        transform.Rotate(-mouseDistance.y * lockRateSpeed * Time.deltaTime,
            mouseDistance.x * lockRateSpeed * Time.deltaTime, rollInput * rollSpeed * Time.deltaTime, Space.Self);
        
        _activeForwardSpeed = Mathf.Lerp(_activeForwardSpeed, Input.GetAxisRaw("Vertical") * forwardSpeed,
            forwardAcceleration * Time.deltaTime);
        _activeStrafeSpeed = Mathf.Lerp(_activeStrafeSpeed, Input.GetAxisRaw("Horizontal") * strafeSpeed,
            strafeAcceleration * Time.deltaTime);
        _activeHoverSpeed = Mathf.Lerp(_activeHoverSpeed, Input.GetAxisRaw("Hover") * hoverSpeed,
            hoverAcceleration * Time.deltaTime);

        transform.position += transform.forward * _activeForwardSpeed * Time.deltaTime;
        transform.position += (transform.right * _activeStrafeSpeed * Time.deltaTime) +
                              (transform.up * _activeHoverSpeed * Time.deltaTime);

    }*/
}



