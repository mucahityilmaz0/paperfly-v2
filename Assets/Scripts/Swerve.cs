using System;
using UnityEngine;

public class Swerve : MonoBehaviour
{
    private SwerveInputSystem _swerveInputSystem;
    [SerializeField] private float swerveSpeed = 0.5f;
    private float _maxSwerveAmountX = 2f;
    private float _maxSwerveAmountY = 3f; 
    private float _swerveAmountX;
    //private float _swerveAmountY;
    

    private void Awake()
    {
        _swerveInputSystem = GetComponent<SwerveInputSystem>();
    }

    private void Update()
    {
        _swerveAmountX= Time.deltaTime * swerveSpeed * _swerveInputSystem.MoveFactorX;
       // _swerveAmountY= Time.deltaTime * swerveSpeed * _swerveInputSystem.MoveFactorY;
        
        Vector3 clampedPosition = gameObject.transform.position;
        clampedPosition.x = Mathf.Clamp(clampedPosition.x, -_maxSwerveAmountX, _maxSwerveAmountX);
        clampedPosition.y = Mathf.Clamp(clampedPosition.y, -_maxSwerveAmountY, _maxSwerveAmountY);
        transform.position = clampedPosition;
        
        transform.Translate(_swerveAmountX,0,0);
        transform.Rotate(0,0,_swerveAmountX);
        //swerveAmount = Mathf.Clamp(swerveAmount,-maxSwerveAmount, maxSwerveAmount);
    }
}
